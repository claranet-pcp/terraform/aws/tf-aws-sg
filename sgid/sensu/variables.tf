variable "name" { default = "sensu" }
variable "vpc_id" { }
variable "security_groups" { }
variable "redis_port" { default = "6379" }
variable "rabbitmq_port" { default = "5672" }
variable "rabbitmq_admin_port" { default = "15672" }
variable "sensu_api_port" { default = "4567" }
