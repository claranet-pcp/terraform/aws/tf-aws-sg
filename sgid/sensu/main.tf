resource "aws_security_group" "sensu" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow incoming redis traffic
  ingress {
    from_port = "${var.redis_port}"
    to_port = "${var.redis_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allows traffic from the SG itself for tcp
  ingress {
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    self = true
  }

  // allows traffic from the SG itself for udp
  ingress {
    from_port = 0
    to_port = 65535
    protocol = "udp"
    self = true
  }

  // allow rabbitmq traffic
  ingress {
    from_port = "${var.rabbitmq_port}"
    to_port = "${var.rabbitmq_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow rabbitmq admin access
  ingress {
    from_port = "${var.rabbitmq_admin_port}"
    to_port = "${var.rabbitmq_admin_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow incoming sensu-api traffic
  ingress {
    from_port = "${var.sensu_api_port}"
    to_port = "${var.sensu_api_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }
}
