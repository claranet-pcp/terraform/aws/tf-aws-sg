variable "name" { default = "redis" }
variable "vpc_id" { }
variable "security_groups" { }
variable "redis_port" { default = "6379" }
