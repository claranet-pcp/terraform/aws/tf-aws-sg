resource "aws_security_group" "redis" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow redis traffic
  ingress {
    from_port = "${var.redis_port}"
    to_port = "${var.redis_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow all outgoing traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
