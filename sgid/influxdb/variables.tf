variable "name" { default = "influxdb" }
variable "vpc_id" { }
variable "security_groups" { }
variable "graphite_port" { default = "2003" }
variable "influxdb_port" { default = "8086" }
variable "influxdb_admin_port" { default = "8083" }
variable "influxdb_cluster_port" { default = "8088" }
