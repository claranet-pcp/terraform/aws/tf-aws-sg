resource "aws_security_group" "influxdb" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow tcp graphite traffic
  ingress {
    from_port = "${var.graphite_port}"
    to_port = "${var.graphite_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow udp graphite traffic
  ingress {
    from_port = "${var.graphite_port}"
    to_port = "${var.graphite_port}"
    protocol = "udp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow influxdb traffic
  ingress {
    from_port = "${var.influxdb_port}"
    to_port = "${var.influxdb_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow influxdb admin
  ingress {
    from_port = "${var.influxdb_admin_port}"
    to_port = "${var.influxdb_admin_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow influxdb cluster
  ingress {
    from_port = "${var.influxdb_cluster_port}"
    to_port = "${var.influxdb_cluster_port}"
    protocol = "tcp"
    self = true
  }

  // allow all outgoing traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
