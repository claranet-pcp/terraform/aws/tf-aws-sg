resource "aws_security_group" "web" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow http traffic
  ingress {
    from_port = "${var.http_port}"
    to_port = "${var.http_port}"
    protocol = "tcp"
    self = "${var.self}"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow https traffic
  ingress {
    from_port = "${var.https_port}"
    to_port = "${var.https_port}"
    protocol = "tcp"
    self = "${var.self}"
    security_groups = ["${split(",", var.security_groups)}"]
  }

}
