variable "name" { default = "web" }
variable "vpc_id" { }
variable "security_groups" { }
variable "self" { default = "true" }
variable "http_port" { default = "80" }
variable "https_port" { default = "443" }
