variable "name" { default = "rabbitmq" }
variable "vpc_id" { }
variable "security_groups" { }
variable "rabbitmq_port" { default = "5672" }
variable "rabbitmq_admin_port" { default = "15672" }
