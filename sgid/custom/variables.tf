variable "name" { default = "mysql" }
variable "vpc_id" { }
variable "security_groups" { }
variable "from_port" { }
variable "to_port" { }
variable "self" { default = "true" }
variable "protocol" { default = "tcp" }
