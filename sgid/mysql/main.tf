resource "aws_security_group" "mysql" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow http traffic
  ingress {
    from_port = "${var.mysql_port}"
    to_port = "${var.mysql_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

}
