variable "name" { default = "mysql" }
variable "vpc_id" { }
variable "security_groups" { }
variable "mysql_port" { default = "3306" }
