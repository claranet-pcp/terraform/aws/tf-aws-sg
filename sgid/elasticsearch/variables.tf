variable "name" { default = "elasticsearch" }
variable "vpc_id" { }
variable "security_groups" { }
variable "elasticsearch_from_port" { default = "9300" }
variable "elasticsearch_to_port" { default = "9305" }
