resource "aws_security_group" "elasticsearch" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allows traffic from the SG itself for tcp
  ingress {
    from_port = "9200"
    to_port = "9200"
    protocol = "tcp"
    self = true
  }

  // allow self elasticsearch transport traffic
  ingress {
    from_port = "${var.elasticsearch_from_port}"
    to_port = "${var.elasticsearch_to_port}"
    protocol = "tcp"
    self = true
  }

  // allow elasticsearch traffic
  ingress {
    from_port = "9200"
    to_port = "9200"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

  // allow elasticsearch transport traffic
  ingress {
    from_port = "${var.elasticsearch_from_port}"
    to_port = "${var.elasticsearch_to_port}"
    protocol = "tcp"
    security_groups = ["${split(",", var.security_groups)}"]
  }

}
