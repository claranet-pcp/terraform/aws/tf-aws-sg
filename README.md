# tf-aws-sg

Create SGs based on service or use the custom sub modules for services not
covered by this module.

**Use a defined service from the module**

```
module "rabbitmq_sg" {

  source = "./tf-aws-sg/cidr/rabbitmq"

  name   = "rabbitmq"
  vpc_id = "${aws_vpc.vpc.id}"

  source_cidrs = "${aws_vpc.vpc.cidr_block}"

}
```

**Use the custom sub module for a service not defined in the module**

```
module "someservice_sg" {

  source = "./tf-aws-sg/cidr/custom"

  name         = "someservice"
  vpc_id       = "${aws_vpc.vpc.id}"
  source_cidrs = "${aws_vpc.vpc.cidr_block}"
  from_port    = "4444"
  to_port      = "4444"

}
```
**Use bstion service from the module**

- optional support for openvpn:

  - set `enable_vpn = true` to get defaults

    ```
    default = {
      protocol = "udp"
      from_port = 1194
      to_port = 1194
      cidr_blocks = "0.0.0.0/0"
    }
    ```
  - set `enable_vpn = true` and custom settings

    ```
    module "bastion_sg" {
      source = "../modules/tf-aws-sg/cidr/bastion"
      name              = "bastion-${var.envtype}"
      vpc_id            = "${module.vpc.vpc_id}"
      source_cidrs      = "${module.vpc.vpc_cidr}"
      enable_openvpn    = true
      openvpn_options   = {
        protocol = "tcp",
        from_port = 1194,
        to_port = 1194,
        cidr_blocks = "10.10.10.10/24"
      }
      allowed_ssh_cidrs = "${var.bashton_claranet_office_ips}"
    }
    ```
