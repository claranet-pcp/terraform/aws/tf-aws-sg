variable "name" { default = "web" }
variable "vpc_id" { }
variable "source_cidrs" { default = "0.0.0.0/0" }
variable "http_port" { default = "80" }
variable "https_port" { default = "443" }
