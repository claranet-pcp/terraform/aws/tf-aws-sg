variable "name" { default = "redis" }
variable "vpc_id" { }
variable "source_cidrs" { default = "0.0.0.0/0" }
variable "redis_port" { default = "6379" }
