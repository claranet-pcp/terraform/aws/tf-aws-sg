resource "aws_security_group" "elasticsearch" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow incoming elasticsearch traffic
  ingress {
    from_port = "${var.elasticsearch_port}"
    to_port = "${var.elasticsearch_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming elasticsearch admin traffic
  ingress {
    from_port = "${var.elasticsearch_admin_port}"
    to_port = "${var.elasticsearch_admin_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow all outgoing traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
