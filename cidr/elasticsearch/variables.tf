variable "name" { default = "elasticsearch" }
variable "vpc_id" { }
variable "source_cidrs" { }
variable "elasticsearch_port" { default = "9200" }
variable "elasticsearch_admin_port" { default = "9300" }
