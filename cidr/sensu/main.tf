resource "aws_security_group" "sensu" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow incoming redis traffic
  ingress {
    from_port = "${var.redis_port}"
    to_port = "${var.redis_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming rabbitmq traffic
  ingress {
    from_port = "${var.rabbitmq_port}"
    to_port = "${var.rabbitmq_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming rabbitmq admin traffic
  ingress {
    from_port = "${var.rabbitmq_admin_port}"
    to_port = "${var.rabbitmq_admin_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming sensu-api traffic
  ingress {
    from_port = "${var.sensu_api_port}"
    to_port = "${var.sensu_api_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow all outgoing traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
