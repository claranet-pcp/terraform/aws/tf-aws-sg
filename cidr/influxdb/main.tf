resource "aws_security_group" "influxdb" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow tcp graphite traffic
  ingress {
    from_port = "${var.graphite_port}"
    to_port = "${var.graphite_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow udp graphite traffic
  ingress {
    from_port = "${var.graphite_port}"
    to_port = "${var.graphite_port}"
    protocol = "udp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming influxdb traffic
  ingress {
    from_port = "${var.influxdb_port}"
    to_port = "${var.influxdb_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming influxdb admin traffic
  ingress {
    from_port = "${var.influxdb_admin_port}"
    to_port = "${var.influxdb_admin_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming influxdb cluster traffic
  ingress {
    from_port = "${var.influxdb_cluster_port}"
    to_port = "${var.influxdb_cluster_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow all outgoing traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
