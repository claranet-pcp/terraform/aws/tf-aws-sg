resource "aws_security_group" "redshift" {
  name        = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id      = "${var.vpc_id}"

  // allow Redshift traffic
  ingress {
    from_port   = "${var.redshift_port}"
    to_port     = "${var.redshift_port}"
    protocol    = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
 }

  // allow all outgoing traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
