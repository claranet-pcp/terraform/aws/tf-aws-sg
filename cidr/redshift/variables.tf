variable "name" { default = "redshift" }
variable "vpc_id" { }
variable "redshift_port" { default = "5439" }
variable "source_cidrs" { }
