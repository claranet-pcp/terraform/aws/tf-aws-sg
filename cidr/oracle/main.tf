resource "aws_security_group" "oracle" {
  name        = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id      = "${var.vpc_id}"
  count       = "${var.enabled ? 1 : 0 }"

  ingress {
    from_port   = "1521"
    to_port     = "1521"
    protocol    = "tcp"
    cidr_blocks = ["${var.source_cidrs}"]
  }

  ingress {
    from_port   = "5500"
    to_port     = "5500"
    protocol    = "tcp"
    cidr_blocks = ["${var.source_cidrs}"]
  }
}
