variable "name" {
  default = "oracle"
}

variable "vpc_id" {
  type = "string"
}
variable "source_cidrs" {
  type = "list"
}

variable "enabled" {
  description = "This allows the flexibility to enable or disable the resources created by this module on a per environment basis."
  default = true
}
