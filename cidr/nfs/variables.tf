variable "name" { default = "nfs" }
variable "vpc_id" { }
variable "source_cidrs" { }
variable "rpcbind_port" { default = "111" }
variable "nfs_server_port" { default = "2049" }
variable "rpc_mountd_port" { default = "20048" }
