resource "aws_security_group" "nfs" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow incoming tcp rpcbind traffic
  ingress {
    from_port = "${var.rpcbind_port}"
    to_port = "${var.rpcbind_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming udp rpcbind traffic
  ingress {
    from_port = "${var.rpcbind_port}"
    to_port = "${var.rpcbind_port}"
    protocol = "udp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming tcp nfs-server traffic
  ingress {
    from_port = "${var.nfs_server_port}"
    to_port = "${var.nfs_server_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming udp nfs-server traffic
  ingress {
    from_port = "${var.nfs_server_port}"
    to_port = "${var.nfs_server_port}"
    protocol = "udp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming tcp rpc_mountd traffic
  ingress {
    from_port = "${var.rpc_mountd_port}"
    to_port = "${var.rpc_mountd_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming udp rpc_mountd traffic
  ingress {
    from_port = "${var.rpc_mountd_port}"
    to_port = "${var.rpc_mountd_port}"
    protocol = "udp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow all outgoing traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
