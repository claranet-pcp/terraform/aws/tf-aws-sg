resource "aws_security_group" "consul" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow traffic for TCP 8300 (Server RPC)
  ingress {
    from_port = 8300
    to_port = 8300
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow traffic for TCP 8301 Serf LAN and TCP 8302 Serf WAN
  ingress {
    from_port = 8301
    to_port = 8302
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow traffic for UDP 8301 Serf LAN and UDP 8302 Serf WAN
  ingress {
    from_port = 8301
    to_port = 8302
    protocol = "udp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow traffic for TCP 8400 (Consul RPC)
  ingress {
    from_port = 8400
    to_port = 8400
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow traffic for TCP 8500 (Consul Web UI)
  ingress {
    from_port = 8500
    to_port = 8500
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow traffic for TCP 8600 (Consul DNS Interface)
  ingress {
    from_port = 8600
    to_port = 8600
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow traffic for UDP 8600 (Consul DNS Interface)
  ingress {
    from_port = 8600
    to_port = 8600
    protocol = "udp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }
}
