resource "aws_security_group" "custom" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow custom traffic
  ingress {
    from_port = "${var.from_port}"
    to_port = "${var.to_port}"
    protocol = "${var.protocol}"
    self = "${var.self}"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

}
