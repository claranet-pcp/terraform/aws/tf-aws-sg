variable "name" { default = "custom" }
variable "vpc_id" { }
variable "from_port" { }
variable "to_port" { }
variable "self" { default = "true" }
variable "source_cidrs" { default = "0.0.0.0/0" }
variable "protocol" { default = "tcp" }
