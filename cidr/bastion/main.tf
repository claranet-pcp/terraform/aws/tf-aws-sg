resource "aws_security_group" "bastion" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"
}

# allow internal traffic
resource "aws_security_group_rule" "internal_traffic" {
    type = "ingress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
    security_group_id = "${aws_security_group.bastion.id}"
}

# allow ssh traffic
resource "aws_security_group_rule" "ssh_from_allowed_cidrs" {
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.allowed_ssh_cidrs)}"]
    security_group_id = "${aws_security_group.bastion.id}"
}

# optional rule for VPN
resource "aws_security_group_rule" "openvpn" {
    count = "${var.enable_openvpn}"
    type = "ingress"
    from_port = "${lookup(var.openvpn_options, "from_port")}"
    to_port = "${lookup(var.openvpn_options, "to_port")}"
    protocol = "${lookup(var.openvpn_options, "protocol")}"
    cidr_blocks = "${split(",", lookup(var.openvpn_options, "cidr_blocks"))}"
    security_group_id = "${aws_security_group.bastion.id}"
}