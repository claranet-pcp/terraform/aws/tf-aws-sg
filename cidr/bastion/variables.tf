variable "name" { default = "bastion" }
variable "vpc_id" { }
variable "source_cidrs" { }
variable "allowed_ssh_cidrs" { default = "88.97.72.136/32,54.76.122.23/32" }
variable "enable_openvpn" {  default = false }
variable "openvpn_options" {
  description = "Defaults for openvpn"
  type = "map"
  default = {
    protocol = "udp"
    from_port = 1194
    to_port = 1194
    cidr_blocks = "0.0.0.0/0"
  }
}