resource "aws_security_group" "zookeeper" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow zookeeper server port
  ingress {
    from_port = "${var.zk_server_port}"
    to_port = "${var.zk_server_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming cluster traffic
  ingress {
    from_port = "${var.zk_cluster_port}"
    to_port = "${var.zk_cluster_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow incoming election traffic
  ingress {
    from_port = "${var.zk_election_port}"
    to_port = "${var.zk_election_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }

  // allow all outgoing traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
