variable "name" { default = "zookeeper" }
variable "vpc_id" { }
variable "source_cidrs" { }
variable "zk_server_port" { default = "2181" }
variable "zk_cluster_port" { default = "2888" }
variable "zk_election_port" { default = "3888" }
