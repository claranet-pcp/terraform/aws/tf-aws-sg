variable "name" {
  default = "activemq"
}

variable "vpc_id" {}

variable "source_cidrs" {
  default = "0.0.0.0/0"
}

variable "activemq_port" {
  default = "61616"
}
