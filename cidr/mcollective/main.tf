resource "aws_security_group" "mcollective" {
  name = "${var.name}"
  description = "Security Group ${var.name}"
  vpc_id = "${var.vpc_id}"

  // allow internal traffic
  ingress {
    from_port = "${var.stomp_port}"
    to_port = "${var.stomp_port}"
    protocol = "tcp"
    cidr_blocks = ["${split(",", var.source_cidrs)}"]
  }
}
