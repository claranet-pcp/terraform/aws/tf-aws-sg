variable "name" { default = "mcollective" }
variable "vpc_id" { }
variable "source_cidrs" { }
variable "stomp_port" { default = "61613" }
